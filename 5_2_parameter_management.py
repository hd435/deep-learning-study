#access parameters
#initialisation
#sharing among models

import torch
from torch import nn

net = nn.Sequential(nn.Linear(4, 8), nn.ReLU(), nn.Linear(8, 1))
X = torch.rand(size=(2, 4))
net(X)


'''
for i in range(len(net)):   
    print(net[i].state_dict())
'''

print(type(net[2].bias))   #torch.nn.parameter.Parameter
print(net[2].bias)  #tensor, require_grad = ...
print(net[2].bias.data)#tensor

net[2].weight.grad == None   #True

print(*[(name, param.shape) for name, param in net.named_parameters()])

#从嵌套块收集参数
def block1():
    return nn.Sequential(nn.Linear(4,8), nn.ReLU(),
                         nn.Linear(8,4), nn.ReLU())

def block2():
    net = nn.Sequential()
    for i in range(4):
        net.add_module(f'block {i}', block1())
    return net

rgnet = nn.Sequential(block2(), nn.Linear(4,1))
print(rgnet)
rgnet[0][1][0].bias.data

#parameters initialisation 
def init_normal(m):
    if type(m) == nn.Linear:
        nn.init.normal_(m.weight, mean=0, std=0.01)
        nn.init.zeros_(m.bias)
net.apply(init_normal)
net[0].weight.data[0], net[0].bias.data[0]


#different init for different layers
def xavier(m):
    if type(m) == nn.Linear:
        nn.init.xavier_uniform_(m.weight)

def init_42(m):
    #if type(m) == nn.Linear:
        nn.init.constant_(m.weight, 42)

net[0].apply(xavier)
net[2].apply(init_42)

#自定义初始化 eg w =u(5, 10), 0, u(-10, -5), p = 1/4,1/2,1/4
def my_init(m):
    if type(m) == nn.Linear:
        print("Init", *[(name, param.shape)
                        for name, param in m.named_parameters()][0])
        nn.init.uniform_(m.weight, -10, 10)    #uniform -10 to 10
        m.weight.data *= m.weight.data.abs() >= 5    #-5 to 5 to zero
        #m.weight.data[0] += 1    #you can set directly
net.apply(my_init)
net[0].weight[:2]


#sharing parameters
# 我们需要给共享层一个名称，以便可以引用它的参数
shared = nn.Linear(8, 8)
net = nn.Sequential(nn.Linear(4, 8), nn.ReLU(),
                    shared, nn.ReLU(),
                    shared, nn.ReLU(),
                    nn.Linear(8, 1))
net(X)
# 检查参数是否相同
print(net[2].weight.data[0] == net[4].weight.data[0])
net[2].weight.data[0, 0] = 100
# 确保它们实际上是同一个对象，而不只是有相同的值
print(net[2].weight.data[0] == net[4].weight.data[0])