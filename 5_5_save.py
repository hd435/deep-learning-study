import torch
from torch import nn
from torch.nn import functional as F

'''
x = torch.arange(4)
torch.save(x, 'x-file')

y = torch.zeros(4)
torch.save([x,y], 'x-files')   #list
x2,y2=torch.load('x-files')

torch.save({'x':x, 'y':y}, 'dict')    #dict
my_dict = torch.load('dict')
'''

'''
深度学习框架提供了内置函数来保存和加载整个网络。 需要注意的一个重要细节是，这将保存模型的参数而不是保存整个模型。 
例如，如果我们有一个3层多层感知机，我们需要单独指定架构。 
因为模型本身可以包含任意代码，所以模型本身难以序列化。 因此，为了恢复模型，我们需要用代码生成架构， 然后从磁盘加载参数
'''

class MLP(nn.Module):
    def __init__(self):
        super().__init__()
        self.hidden = nn.Linear(20, 256)
        self.output = nn.Linear(256, 10)

    def forward(self, x):
        return self.output(F.relu(self.hidden(x)))

net = MLP()
X = torch.randn(size=(2, 20))
Y = net(X)

torch.save(net.state_dict(), 'net_param')
clone = MLP()
clone.load_State_dict(torch.load('net_param'))
clone.eval()

#or
torch.save(net, 'net.pt')
net2 = torch.load('net.pt')